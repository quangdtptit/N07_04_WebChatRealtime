<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>Đăng nhập</title>
   <!--Made with love by Mutiullah Samim -->
   
	<!--Bootsrap 4 CDN-->
	<link rel="stylesheet" href="<c:url value="/template/css/bootstrap.min.css"/>">    
    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="<c:url value="/template/css/all.css"/>">
	<!--Custom styles-->
	<link rel="stylesheet" href="<c:url value="/template/css/login.css"/>">
</head>
<body>
<div class="container">
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			<div class="card-header">
				<h3>Đăng nhập</h3>
			</div>
			<div class="card-body">
				<form action="<c:url value="/dang-nhap"/>" method="post">
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input type="text" name="userName" class="form-control" placeholder="Tài khoản ..." autocomplete="off">
						
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="password" name="password" class="form-control" placeholder="Mật khẩu ..." >
					</div>
					<div class="row align-items-center remember">
						<input type="checkbox">Nhớ mật khẩu
					</div>
					<div class="form-group">
						<input type="submit" value="Đăng nhập" class="btn float-right login_btn">
					</div>
				</form>
			</div>
			<div class="card-footer">
				<div class="d-flex justify-content-center links">
					Tạo tài khoản ?<a href="#">Đăng ký</a>
				</div>
				<div class="d-flex justify-content-center">
					<a href="#">Quên mật khẩu ?</a>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>