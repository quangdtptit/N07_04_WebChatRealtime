<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>

<head>
    <title>Chat - Chit</title>
    <link rel="stylesheet" href="<c:url value="/template/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/template/css/all.css"/>">
    <link rel="stylesheet" href="<c:url value="/template/css/chat.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/template/css/jquery.mCustomScrollbar.min.css"/>">
    <script src="<c:url value="/template/js/jquery.min.js"/>"></script>
    <script src="<c:url value="/template/js/popper.min.js"/>"></script>
    <script src="<c:url value="/template/js/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/template/js/jquery.mCustomScrollbar.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/template/js/chat.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/template/js/model.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/template/js/moment.js"/>"></script>
</head>

<body>
    <div class="container-fluid h-100">
        <div class="row justify-content-center h-100">
            <div class="col-md-4 col-xl-3 chat">
                <div class="card mb-sm-3 mb-md-0 contacts_card">
                    <div class="card-header">
                        <div class="input-group">
                            <input type="text" placeholder="Search..." name="" class="form-control search">
                            <div class="input-group-prepend">
                                <span class="input-group-text search_btn"><i
									class="fas fa-search"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="card-body contacts_body">
                        <ul class="contacts" id="listOnline">
                            <!-- Append user online -->
                            
                        </ul>
                    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
            <div class="col-md-8 col-xl-6 chat">
                <div class="card">
                    <div class="card-header msg_head">
                        <div class="d-flex bd-highlight">
                            <div class="img_cont">
                                <img id="imageChat" src="<c:url value= "/template/images/avt.jpg"/>" class="rounded-circle user_img"> <span class="online_icon"></span>
                            </div>
                            <div class="user_info">
                                <span id="chatWith">${USERMODEL.fullName}</span>
                            </div>
                        </div>
                        <span id="action_menu_btn"><i class="fas fa-ellipsis-v"></i></span>
                        <div class="action_menu">
                            <ul>
                                <li id="fullName"><i class="fas fa-user-circle"></i>${USERMODEL.fullName}</li>
                                <li><i class="fas fa-plus"></i><a href="javascript:;" data-toggle="modal" data-target="#myModal" id="isGr">Tạo nhóm chat</a></li>
                                <li><i class="fas fa-ban"></i><a href="<c:url value="/dang-nhap?action=logout"/>">Đăng xuất</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body msg_card_body" id="displayChat">
                        <!-- Append Chat -->
                        <%-- <c:if test="${not empty MESSAGECHATS}">
                            <c:forEach var="item" items="${MESSAGECHATS}">
                                <div class="d-flex justify-content-end mb-4">
                                    <div class="msg_cotainer_send">
                                        ${item.content}
                                        <!-- <span class="msg_time_send">8:55 AM, Today</span> -->
                                    </div>
                                    <div class="img_cont_msg">
                                        <img src="<c:url value= "/template/images/avt.jpg"/>" class="rounded-circle user_img_msg">
                                    </div>
                                </div>
                            </c:forEach>
                        </c:if> --%>
                    </div>
                    <%-- <form id="uploadFile" method="post" action="<c:url value="/uploadFileServlet"/>" enctype="multipart/form-data"> --%>
	                    <div class="card-footer">
	                        <div class="input-group">
	                        	<div class="input-group-append">
	                                <span onclick="document.querySelector('#inputFile').click();" class="input-group-text attach_btn"><i class="fas fa-paperclip"></i>                        
	                                	<input id="inputFile" type="file" name="name" style="display: none;" /> 
	                                </span>
	                            </div>
	                            <textarea id="txtMessage" name="" class="form-control type_msg" placeholder="Type your message..."></textarea>
	                        </div>
	                    </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>
        <input type="hidden" id="idSession" value="${USERMODEL.fullName}" />
        <input type="hidden" id="idUserSend" value="${USERMODEL.id}" />
    </div>
    
    <!-- The Modal -->
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Nhóm</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
				<form id = "formAddGr" method="post">
	                <!-- Modal body -->
	                <div class="modal-body">
	                    <!-- Table  -->
	                        <div class="form-group">
	                            <input type="text" class="form-control" name="nameGroup" id="nameGroup" placeholder="Điền tên nhóm ...">
	                        </div>
	                        <table class="table table-bordered" id="listFriend">
	                            <!-- Table head -->
	                            <thead>
	                                <tr>
	                                    <th>Tùy chọn</th>
	                                    <th>Bạn bè</th>
	                                </tr>
	                            </thead>
	                            <!-- Table head -->
	
	                            <!-- Table body -->
	                            <tbody>
	                                <!-- add friend -->
	                            </tbody>
	                            <!-- Table body -->
	                        </table>
	                        <!-- Table  -->
	                </div>
	                <!-- Modal footer -->
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnAddGr">Tạo nhóm</button>
	                </div>
                </form>
            </div>
        </div>
    </div>
    
    <div style="display: none" id="listGr">
    	<c:if test="${not empty GRCHATS}">
              <c:forEach var="item" items="${GRCHATS}">
                     <li data-id="gr-${item.id}" style="cursor: pointer;" class="">
                            			<div class="d-flex bd-highlight">
                            				<div class="img_cont">
                            					<img src="/template/images/group.jpg" class="rounded-circle user_img">
                            					<span class="online_icon"></span>
                            				</div>
	                            			<div class="user_info">
		                            			<span id="name-id-gr-${item.id}">${item.name}</span>
		                            			<p></p>
	                            			</div>
                            			</div>
                            		</li>
              </c:forEach>
          </c:if>
    </div>

    <script type="text/javascript" src="<c:url value= "/template/js/client.js"/>"></script>
</body>

</html>