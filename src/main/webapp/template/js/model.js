function MessageChat() {
	this.id = null;
	this.idUserSend = null;
	this.idUserReceive = null;
	this.content = null;
	this.type = null;
	this.fullName = null;
	this.createTime = null;
	this.createBy = null;
	this.roomChat = null;
	//this.checkNotify = null;
}

function RoomChat() {
	this.id = null;
	this.idUserTwo = null;
	this.idUserOne = null;
	this.createTime = null;
	this.createBy = null;
}

function GroupChat() {
	this.id = null;
	this.name = null;
	this.member = null;
	this.type = null;
	this.createTime = null;
	this.createBy = null;
}

