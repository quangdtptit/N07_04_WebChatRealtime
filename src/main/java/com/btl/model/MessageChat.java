package com.btl.model;

public class MessageChat extends Base {

	private Long idUserSend;
	private Long idUserReceive;
	private String content;
	private Integer type;
	private String fullName;
	//private String checkNotify;
	private RoomChat roomChat;
	private Long idGroupChat;

	public Long getIdUserSend() {
		return idUserSend;
	}

	public void setIdUserSend(Long idUserSend) {
		this.idUserSend = idUserSend;
	}

	public Long getIdUserReceive() {
		return idUserReceive;
	}

	public void setIdUserReceive(Long idUserReceive) {
		this.idUserReceive = idUserReceive;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public RoomChat getRoomChat() {
		return roomChat;
	}

	public void setRoomChat(RoomChat roomChat) {
		this.roomChat = roomChat;
	}

	public Long getIdGroupChat() {
		return idGroupChat;
	}

	public void setIdGroupChat(Long idGroupChat) {
		this.idGroupChat = idGroupChat;
	}

//	public String getCheckNotify() {
//		return checkNotify;
//	}
//
//	public void setCheckNotify(String checkNotify) {
//		this.checkNotify = checkNotify;
//	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fullName == null) ? 0 : fullName.hashCode());
		result = prime * result + ((idUserSend == null) ? 0 : idUserSend.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessageChat other = (MessageChat) obj;
		if (fullName == null) {
			if (other.fullName != null)
				return false;
		} else if (!fullName.equals(other.fullName))
			return false;
		if (idUserSend == null) {
			if (other.idUserSend != null)
				return false;
		} else if (!idUserSend.equals(other.idUserSend))
			return false;
		return true;
	}

}
