package com.btl.model;

public class RoomChat extends Base {
	private Long idUserOne;
	private Long idUserTwo;
	private int page = 1;

	public Long getIdUserOne() {
		return idUserOne;
	}

	public void setIdUserOne(Long idUserOne) {
		this.idUserOne = idUserOne;
	}

	public Long getIdUserTwo() {
		return idUserTwo;
	}

	public void setIdUserTwo(Long idUserTwo) {
		this.idUserTwo = idUserTwo;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

}
