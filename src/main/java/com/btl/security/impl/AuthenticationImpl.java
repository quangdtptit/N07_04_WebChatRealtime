package com.btl.security.impl;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import com.btl.model.User;
import com.btl.security.Authentication;
import com.btl.service.UserService;
import com.btl.utils.SessionUtil;

public class AuthenticationImpl implements Authentication {

	@Inject
	private UserService userService;

	@Override
	public String urlRediect(HttpServletRequest req, User user) {
		
		User result = null;
		try {
			result = userService.isExits(user);
		} catch (NullPointerException e) {
			return "/dang-nhap";
		}
		if (result != null) {
			SessionUtil.getInstance().putValue(req, "USERMODEL", result);
			return "/messages/t/a-" + result.getId();
		}
		return "/dang-nhap";
	}
}
