package com.btl.controller;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btl.model.User;
import com.btl.security.Authentication;
import com.btl.utils.PojoUtil;
import com.btl.utils.SessionUtil;

@WebServlet(urlPatterns = { "/dang-nhap" })
public class LoginController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Inject
	private Authentication security;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String logout = req.getParameter("action");
		if (logout != null) {
			SessionUtil.getInstance().removeValue(req, "USERMODEL");
		}
		RequestDispatcher rd = req.getRequestDispatcher("/views/login.jsp");
		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		User user = PojoUtil.toModel(User.class, req);
		resp.sendRedirect(req.getContextPath() + security.urlRediect(req, user));
	}

}
