package com.btl.controller.web;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btl.model.GroupChat;
import com.btl.model.User;
import com.btl.service.GroupChatService;
import com.btl.utils.SessionUtil;

@WebServlet(urlPatterns = { "/messages/t/*" })
public class HomeController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/*
	 * @Inject private MessageChatService mesService;
	 */

	@Inject
	private GroupChatService grService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		/** chức năng load tin nhắn khi refesh lại trang - đang lỗi
		 * List<MessageChat> messageChats = null; Long id = getId(req); if (id != null)
		 * { RoomChat roomChat = new RoomChat(); roomChat.setIdUserOne(((User)
		 * SessionUtil.getInstance().getValue(req, "USERMODEL")).getId());
		 * roomChat.setIdUserTwo(id); messageChats =
		 * mesService.findAllByRoomChat(roomChat); } req.setAttribute("MESSAGECHATS",
		 * messageChats);
		 */
		
		List<GroupChat> grChats = null;
		grChats = getGroupBySession(req);
		req.setAttribute("GRCHATS", grChats);
		
		RequestDispatcher rd = req.getRequestDispatcher("/views/chat.jsp");
		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	}

	/*
	 * private Long getId(HttpServletRequest req) { String pathInfor =
	 * req.getPathInfo(); String[] arrs = pathInfor.split("/"); if (arrs.length ==
	 * 2) { try { return Long.parseLong(arrs[1].split("-")[1]); } catch
	 * (NumberFormatException e) {
	 * System.err.println("Error convert String to Long"); return null; } } return
	 * null; }
	 */

	private List<GroupChat> getGroupBySession(HttpServletRequest request) {
		List<GroupChat> groupChats = null;
		groupChats = grService
				.findByAMember(((User) SessionUtil.getInstance().getValue(request, "USERMODEL")).getId() + "");
		return groupChats;
	}
}
