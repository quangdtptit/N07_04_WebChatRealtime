package com.btl.controller.api;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btl.model.GroupChat;
import com.btl.model.MessageChat;
import com.btl.model.RoomChat;
import com.btl.service.MessageChatService;
import com.btl.utils.HttpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebServlet(urlPatterns = { "/api/messages/*" })
public class MessageChatApi extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Inject
	private MessageChatService mesService;

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<MessageChat> list = null;
		String pathInfo = req.getPathInfo();
		ObjectMapper objectMapper = new ObjectMapper();
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		RoomChat roomChat = HttpUtil.of(req.getReader()).toModel(RoomChat.class);
		if (pathInfo == null || pathInfo.equals("/")) {
			list = mesService.findAll();
		} else {
			String[] strs = pathInfo.split("/");
			if (strs.length == 2) {
				try {
					roomChat.setIdUserOne(Long.parseLong(strs[1]));
					//System.out.println("ID1 : " + roomChat.getIdUserOne() + "\nID2 : " + roomChat.getIdUserTwo() +"\n : "+roomChat.getPage());
					//list = mesService.findAllByRoomChat(roomChat); // findAll
					list = mesService.findAllByRoomChatPaging(roomChat);
				} catch (NumberFormatException e) {
					return;
				}
			}
		}
		objectMapper.writeValue(resp.getOutputStream(), list);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<MessageChat> list = null;
		String pathInfo = req.getPathInfo();
		ObjectMapper objectMapper = new ObjectMapper();
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		GroupChat groupChat = HttpUtil.of(req.getReader()).toModel(GroupChat.class);
		if(groupChat == null) {
			groupChat = new GroupChat();
		}
		if (pathInfo == null || pathInfo.equals("/")) {

		} else {
			String[] strs = pathInfo.split("/");
			if (strs.length == 2) {
				try {
					groupChat.setId(Long.parseLong(strs[1]));
					list = mesService.findAllByGroupChatPaging(groupChat);
				} catch (NumberFormatException e) {
					return;
				}
			}
		}
		objectMapper.writeValue(resp.getOutputStream(), list);
	}
}
