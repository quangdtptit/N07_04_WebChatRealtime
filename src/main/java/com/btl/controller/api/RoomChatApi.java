package com.btl.controller.api;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.btl.model.RoomChat;
import com.btl.service.RoomChatService;
import com.btl.utils.HttpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebServlet(urlPatterns = { "/api/roomchat/*" })
public class RoomChatApi extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Inject
	private RoomChatService roomService;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pathInfo = req.getPathInfo();
		ObjectMapper objectMapper = new ObjectMapper();
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("application/json");
		RoomChat roomChat = HttpUtil.of(req.getReader()).toModel(RoomChat.class);
		if (pathInfo == null || pathInfo.equals("/")) {
			return;
		} else {
			String[] strs = pathInfo.split("/");
			if (strs.length == 2) {
				RoomChat roomExits = roomService.findOneByTwoUser(Long.parseLong(strs[1]), roomChat.getIdUserTwo());
				if (roomExits != null) {
					objectMapper.writeValue(resp.getOutputStream(), roomExits.getId());
					return;
				} else {
					roomChat.setIdUserOne(Long.parseLong(strs[1]));
				}
			}
		}
		roomChat = roomService.save(roomChat);
		objectMapper.writeValue(resp.getOutputStream(), roomChat);
	}
}
