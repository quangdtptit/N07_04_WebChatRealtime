package com.btl.code;

import java.io.IOException;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

import org.codehaus.jackson.map.ObjectMapper;

import com.btl.model.MessageChat;

public class MessageChatDecode implements Decoder.Text<MessageChat> {

	@Override
	public void init(EndpointConfig config) {
	}

	@Override
	public void destroy() {
	}

	@Override
	public MessageChat decode(String s) throws DecodeException {
		MessageChat messageChat = null;
		try {
			messageChat = new ObjectMapper().readValue(s, MessageChat.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return messageChat;
	}

	@Override	
	public boolean willDecode(String s) {
		boolean flag = true;
		try {
			new ObjectMapper().readValue(s, MessageChat.class);
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}

}
