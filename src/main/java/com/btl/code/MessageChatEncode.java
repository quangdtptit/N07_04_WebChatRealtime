package com.btl.code;

import java.io.IOException;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

import org.codehaus.jackson.map.ObjectMapper;

import com.btl.model.MessageChat;

public class MessageChatEncode implements Encoder.Text<MessageChat> {

	@Override
	public void init(EndpointConfig config) {
	}

	@Override
	public void destroy() {
	}

	@Override
	public String encode(MessageChat object) throws EncodeException {
		ObjectMapper mapper = new ObjectMapper();
		String jsonData = null;
		try {
			jsonData = mapper.writeValueAsString(object);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonData;
	}

}
