package com.btl.paging;

public class PageRequest implements Pageble {

	private Integer page;
	private Integer maxPageItem;

	public PageRequest(Integer page, Integer maxPageItem) {
		this.page = page;
		this.maxPageItem = maxPageItem;
	}

	@Override
	public Integer getPage() {
		return this.page;
	}

	@Override
	public Integer getOffset() {
		if (this.page != null && this.maxPageItem != null)
			return (this.page - 1) * this.maxPageItem;
		return null;
	}

	@Override
	public Integer getLimit() {
		return this.getMaxPageItem();
	}

	public Integer getMaxPageItem() {
		return maxPageItem;
	}

	public void setMaxPageItem(Integer maxPageItem) {
		this.maxPageItem = maxPageItem;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

}
