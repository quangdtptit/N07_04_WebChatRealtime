package com.btl.paging;

public interface Pageble {
	Integer getPage();

	Integer getOffset();

	Integer getLimit();
}
