package com.btl.dao;

import java.util.List;

import com.btl.model.User;

public interface UserDao extends GennericDao<User> {

	public User isExits(String userName, Integer status);
	
	public List<User> findAll();

}
