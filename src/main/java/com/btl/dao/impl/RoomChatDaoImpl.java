package com.btl.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.btl.dao.RoomChatDao;
import com.btl.mapper.impl.RoomChatMapper;
import com.btl.model.RoomChat;

public class RoomChatDaoImpl extends AbstractDao<RoomChat> implements RoomChatDao {

	@Override
	public RoomChat findOneByIdUserOneAndIdUserTwo(Long idUserOne, Long idUserTwo) {
		StringBuilder sql = new StringBuilder("SELECT * FROM roomchat ");
		sql.append("WHERE (idUserOne = ? AND idUserTwo = ?) ");
		sql.append("OR (idUserTwo = ? AND idUserOne = ?)");
		List<RoomChat> list = query(sql.toString(), new RoomChatMapper(), idUserOne, idUserTwo, idUserOne, idUserTwo);
		return !list.isEmpty() ? list.get(0) : null;
	}

	@Override
	public RoomChat findOneById(Long id) {
		StringBuilder sql = new StringBuilder("SELECT * FROM roomchat ");
		sql.append("WHERE id = ?");
		List<RoomChat> list = new ArrayList<>();
		list = query(sql.toString(), new RoomChatMapper(), id);
		return !list.isEmpty() ? list.get(0) : null;
	}

	@Override
	public Long insert(RoomChat roomChat) {
		StringBuilder sql = new StringBuilder("INSERT INTO roomchat(idUserOne,idUserTwo,createTime,createBy) ");
		sql.append("VALUES(?,?,?,?)");
		Long id = insert(sql.toString(), roomChat.getIdUserOne(), roomChat.getIdUserTwo(), roomChat.getCreateTime(),
				roomChat.getCreateBy());
		return id;
	}

}
