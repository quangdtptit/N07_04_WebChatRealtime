package com.btl.dao.impl;

import java.util.List;

import com.btl.dao.UserDao;
import com.btl.mapper.impl.UserMappper;
import com.btl.model.User;

public class UserDaoImpl extends AbstractDao<User> implements UserDao {

	@Override
	public User isExits(String userName, Integer status) {
		String sql = "SELECT * FROM user WHERE username = ? AND status = ?";
		List<User> list = this.query(sql, new UserMappper(), userName, status);
		return !list.isEmpty() ? list.get(0) : null;
	}

	@Override
	public List<User> findAll() {
		String sql = "SELECT * FROM user WHERE status = 1";
		List<User> list = this.query(sql, new UserMappper());
		return list;
	}

}
