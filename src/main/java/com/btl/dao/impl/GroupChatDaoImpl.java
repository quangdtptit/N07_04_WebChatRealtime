package com.btl.dao.impl;

import java.util.List;

import com.btl.dao.GroupChatDao;
import com.btl.mapper.impl.GroupChatMapper;
import com.btl.model.GroupChat;

public class GroupChatDaoImpl extends AbstractDao<GroupChat> implements GroupChatDao {

	@Override
	public Long insert(GroupChat groupChat) {
		StringBuilder sql = new StringBuilder("INSERT INTO groupchat(name,member,type,createtime,createby) ");
		sql.append("VALUES(?,?,?,?,?)");
		Long id = this.insert(sql.toString(), groupChat.getName(), groupChat.getMember(), groupChat.getType(),
				groupChat.getCreateTime(), groupChat.getCreateBy());
		return id;
	}

	@Override
	public List<GroupChat> findByMember(String aMember) {
		StringBuilder sql = new StringBuilder("SELECT * FROM groupchat ");
		sql.append("WHERE type = 2 ");
		sql.append("AND member LIKE ?");
		String id = "%" + aMember + "%";
		List<GroupChat> list = this.query(sql.toString(), new GroupChatMapper(), id);
		return list;
	}

	@Override
	public GroupChat findById(Long id) {
		StringBuilder sql = new StringBuilder("SELECT * FROM groupchat ");
		sql.append("WHERE id = ?");
		List<GroupChat> list = this.query(sql.toString(), new GroupChatMapper(), id);
		return !list.isEmpty() ? list.get(0) : null;
	}

	public static void main(String[] args) {
		GroupChatDao dao = new GroupChatDaoImpl();
		System.out.println(dao.findById(1L).getName());
	}

}
