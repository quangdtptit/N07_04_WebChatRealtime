package com.btl.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.btl.dao.MessageChatDao;
import com.btl.mapper.impl.MessageChatMapper;
import com.btl.model.MessageChat;

public class MessageChatDaoImpl extends AbstractDao<MessageChat> implements MessageChatDao {

	@Override
	public List<MessageChat> findAllByRoomChat(Long idRoomChat) {
		StringBuilder sql = new StringBuilder("SELECT * FROM messagechat AS m ");
		sql.append("JOIN roomchat AS r ON m.idRoomChat = r.id ");
		sql.append("WHERE r.id = ? ");
		sql.append("ORDER BY m.createTime ASC");
		List<MessageChat> list = new ArrayList<>();
		list = query(sql.toString(), new MessageChatMapper(), idRoomChat);
		return !list.isEmpty() ? list : null;
	}

	// Test
	@Override
	public List<MessageChat> findAll() {
		String sql = "SELECT * FROM messagechat";
		List<MessageChat> list = new ArrayList<>();
		list = query(sql.toString(), new MessageChatMapper());
		return list;
	}

	@Override
	public Long insert(MessageChat messageChat) {
		StringBuilder sql = new StringBuilder(
				"INSERT INTO messagechat(idUserSend,content,type,createTime,idRoomChat,idGroupChat) ");
		sql.append("VALUES(?,?,?,?,?,?)");
		Long idRoom = null;
		try {
			idRoom = messageChat.getRoomChat().getId();
		} catch (NullPointerException e) {
			idRoom = null;
		}
		Long id = insert(sql.toString(), messageChat.getIdUserSend(), messageChat.getContent(), messageChat.getType(),
				messageChat.getCreateTime(), idRoom, messageChat.getIdGroupChat());
		return id;
	}

	@Override
	public List<MessageChat> findAllByRoomChat(Long idUserOne, Long idUserTwo) {
		StringBuilder sql = new StringBuilder("SELECT * FROM messagechat AS m ");
		sql.append("JOIN roomchat AS r ON m.idRoomChat = r.id ");
		sql.append("WHERE (r.idUserOne = ? AND r.idUserTwo = ?) ");
		sql.append("OR (r.idUserOne = ? AND r.idUserTwo = ?) ");
		sql.append("ORDER BY m.createTime ASC");
		List<MessageChat> list = new ArrayList<>();
		list = query(sql.toString(), new MessageChatMapper(), idUserOne, idUserTwo, idUserTwo, idUserOne);
		return !list.isEmpty() ? list : null;
	}

	@Override
	public MessageChat findOneById(Long id) {
		String sql = "SELECT * FROM messagechat WHERE id = ?";
		List<MessageChat> list = query(sql, new MessageChatMapper(), id);
		return !list.isEmpty() ? list.get(0) : null;
	}

	@Override
	public List<MessageChat> findByRoomChatOnPage(Long idUserOne, Long idUserTwo, int page) {
		StringBuilder sql = new StringBuilder("SELECT TEMP.* FROM ");
		sql.append("(SELECT m.* FROM messagechat AS m ");
		sql.append("JOIN roomchat AS r ON m.idRoomChat = r.id ");
		sql.append("WHERE (r.idUserOne = ? AND r.idUserTwo = ?) ");
		sql.append("OR (r.idUserOne = ? AND r.idUserTwo = ?) ");
		sql.append("ORDER BY m.createTime DESC ");
		sql.append("LIMIT ?,10) AS TEMP ");
		sql.append("ORDER BY TEMP.createTime ASC");
		List<MessageChat> list = new ArrayList<>();
		list = query(sql.toString(), new MessageChatMapper(), idUserOne, idUserTwo, idUserTwo, idUserOne, page - 1);
		return !list.isEmpty() ? list : null;
	}

	@Override
	public Integer getCount(Long idRoomChat) {
		StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM messagechat ");
		sql.append("WHERE idroomchat = ? ");
		return count(sql.toString(), idRoomChat);
	}

	@Override
	public List<MessageChat> findAllByGroupChat(Long idGroupChat) {
		StringBuilder sql = new StringBuilder("SELECT m.idUserSend,m.content,m.type,m.createTime,u.fullname ");
		sql.append("FROM messagechat AS m,user AS u ");
		sql.append("WHERE m.idGroupChat = ? ");
		sql.append("AND u.id = m.idUserSend ");
		sql.append("ORDER BY m.createTime ASC");
		List<MessageChat> list = new ArrayList<>();
		list = query(sql.toString(), new MessageChatMapper(), idGroupChat);
		return !list.isEmpty() ? list : null;
	}

	@Override
	public List<MessageChat> findAllByGroupChatOnPage(Long idGroupChat, int page) {
		StringBuilder sql = new StringBuilder("SELECT TEMP.* FROM (");
		sql.append("SELECT m.idUserSend,m.content,m.type,m.createTime,u.fullname ");
		sql.append("FROM messagechat AS m,user AS u ");
		sql.append("WHERE m.idGroupChat = ? ");
		sql.append("AND u.id = m.idUserSend ");
		sql.append("ORDER BY m.createTime DESC ");
		sql.append("LIMIT ?,10) AS TEMP ");
		sql.append("ORDER BY TEMP.createTime ASC");
		List<MessageChat> list = new ArrayList<>();
		list = query(sql.toString(), new MessageChatMapper(), idGroupChat, page - 1);
		return !list.isEmpty() ? list : null;
	}

	public static void main(String[] args) {
		MessageChatDao dao = new MessageChatDaoImpl();
		List<MessageChat> list = dao.findAllByGroupChatOnPage(13L, 1);
		for (MessageChat messageChat : list) {
			System.out.println(messageChat.getContent());
		}
	}

}
