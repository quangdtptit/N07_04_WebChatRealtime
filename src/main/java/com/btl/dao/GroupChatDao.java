package com.btl.dao;

import java.util.List;

import com.btl.model.GroupChat;

public interface GroupChatDao extends GennericDao<GroupChat>{
	public Long insert(GroupChat groupChat);
	
	public List<GroupChat> findByMember(String aMember);
	
	public GroupChat findById(Long id);
}
