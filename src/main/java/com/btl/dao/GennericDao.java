package com.btl.dao;

import java.util.List;

import com.btl.mapper.IMapper;

public interface GennericDao<T> {
	List<T> query(String sql, IMapper<T> rowMapper, Object... parameters);

	void update(String sql, Object... parameters);

	Long insert(String sql, Object... parameters);

	int count(String sql, Object... parameters);
}
