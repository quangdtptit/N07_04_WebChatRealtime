package com.btl.dao;

import com.btl.model.RoomChat;

public interface RoomChatDao extends GennericDao<RoomChat> {

	public RoomChat findOneByIdUserOneAndIdUserTwo(Long idUserOne, Long idUserTwo);

	public RoomChat findOneById(Long id);
	
	public Long insert(RoomChat roomChat);
}
