package com.btl.dao;

import java.util.List;

import com.btl.model.MessageChat;

public interface MessageChatDao extends GennericDao<MessageChat> {

	public List<MessageChat> findAllByRoomChat(Long idRoomChat);

	public List<MessageChat> findAll();

	public Long insert(MessageChat messageChat);

	public List<MessageChat> findAllByRoomChat(Long idUserOne, Long idUserTwo);
	
	public List<MessageChat> findByRoomChatOnPage(Long idUserOne, Long idUserTwo,int page);

	public MessageChat findOneById(Long id);

	public Integer getCount(Long idRoomChat);
	
	public List<MessageChat> findAllByGroupChat(Long idGroupChat);
	
	public List<MessageChat> findAllByGroupChatOnPage(Long idGroupChat, int page);
}
