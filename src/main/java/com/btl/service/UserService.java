package com.btl.service;

import java.util.List;

import com.btl.model.User;

public interface UserService {
	public User isExits(User user);

	public List<User> findAll();
}
