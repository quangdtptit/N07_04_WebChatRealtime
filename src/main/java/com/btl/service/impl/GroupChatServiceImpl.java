package com.btl.service.impl;

import java.util.List;

import javax.inject.Inject;

import com.btl.dao.GroupChatDao;
import com.btl.model.GroupChat;
import com.btl.service.GroupChatService;

public class GroupChatServiceImpl implements GroupChatService {

	@Inject
	private GroupChatDao groupChatDao;

	@Override
	public GroupChat save(GroupChat groupChat) {
		Long id = groupChatDao.insert(groupChat);
		return groupChatDao.findById(id);
	}

	@Override
	public List<GroupChat> findByAMember(String aMember) {
		return groupChatDao.findByMember(aMember);
	}

}
