package com.btl.service.impl;

import java.sql.Timestamp;

import javax.inject.Inject;

import com.btl.dao.RoomChatDao;
import com.btl.model.RoomChat;
import com.btl.service.RoomChatService;

public class RoomChatServiceImpl implements RoomChatService {

	@Inject
	private RoomChatDao roomDao;

	@Override
	public RoomChat findOneById(Long id) {
		return roomDao.findOneById(id);
	}

	@Override
	public RoomChat findOneByTwoUser(Long userOne, Long userTwo) {
		return roomDao.findOneByIdUserOneAndIdUserTwo(userOne, userTwo);
	}

	@Override
	public RoomChat save(RoomChat roomChat) {
		roomChat.setCreateTime(new Timestamp(System.currentTimeMillis()));
		roomChat.setCreateBy("");
		Long id = roomDao.insert(roomChat);
		return roomDao.findOneById(id);
	}

}
