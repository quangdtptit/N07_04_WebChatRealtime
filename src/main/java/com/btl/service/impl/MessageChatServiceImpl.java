package com.btl.service.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.inject.Inject;

import com.btl.dao.MessageChatDao;
import com.btl.model.GroupChat;
import com.btl.model.MessageChat;
import com.btl.model.RoomChat;
import com.btl.service.MessageChatService;

public class MessageChatServiceImpl implements MessageChatService {

	@Inject
	private MessageChatDao messDao;

	@Override
	public List<MessageChat> findAllByRoomChat(RoomChat roomChat) {
		return messDao.findAllByRoomChat(roomChat.getIdUserOne(), roomChat.getIdUserTwo());
	}

	@Override
	public List<MessageChat> findAll() {
		return messDao.findAll();
	}

	@Override
	public MessageChat save(MessageChat messageChat) {
		messageChat.setCreateTime(new Timestamp(System.currentTimeMillis()));
		messageChat.setType(1);
		Long id = messDao.insert(messageChat);
		return messDao.findOneById(id);
	}

	@Override
	public List<MessageChat> findAllByGroupChat(GroupChat groupChat) {
		return messDao.findAllByGroupChat(groupChat.getId());
	}

	@Override
	public List<MessageChat> findAllByRoomChatPaging(RoomChat roomChat) {
		return messDao.findByRoomChatOnPage(roomChat.getIdUserOne(),roomChat.getIdUserTwo(),roomChat.getPage());
	}

	@Override
	public List<MessageChat> findAllByGroupChatPaging(GroupChat groupChat) {
		return messDao.findAllByGroupChatOnPage(groupChat.getId(), groupChat.getPage());
	}

}
