package com.btl.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.mindrot.jbcrypt.BCrypt;

import com.btl.dao.UserDao;
import com.btl.model.User;
import com.btl.service.UserService;

public class UserServiceImpl implements UserService {

	@Inject
	private UserDao userDao;

	@Override
	public User isExits(User user) {
		User result = userDao.isExits(user.getUserName(), 1);
		if (checkPass(user.getPassword(), result.getPassword()))
			return result;
		return null;
	}

	private boolean checkPass(String password, String salt) {
		try {
			if (BCrypt.checkpw(password, salt) == true)
				return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
		return false;
	}

	@Override
	public List<User> findAll() {
		return userDao.findAll();
	}
}
