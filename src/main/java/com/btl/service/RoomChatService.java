package com.btl.service;

import com.btl.model.RoomChat;

public interface RoomChatService {
	public RoomChat findOneById(Long id);
	
	public RoomChat findOneByTwoUser(Long userOne , Long userTwo);
	
	public RoomChat save(RoomChat roomChat);
}
