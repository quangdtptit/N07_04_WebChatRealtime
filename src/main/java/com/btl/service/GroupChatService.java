package com.btl.service;

import java.util.List;

import com.btl.model.GroupChat;

public interface GroupChatService {
	public GroupChat save(GroupChat groupChat);
	
	public List<GroupChat> findByAMember(String aMember);

}
