package com.btl.service;

import java.util.List;

import com.btl.model.GroupChat;
import com.btl.model.MessageChat;
import com.btl.model.RoomChat;

public interface MessageChatService {
	public List<MessageChat> findAllByRoomChat(RoomChat roomChat);
	
	public List<MessageChat> findAllByRoomChatPaging(RoomChat roomChat);

	public List<MessageChat> findAll();

	public MessageChat save(MessageChat messageChat);
	
	public List<MessageChat> findAllByGroupChat(GroupChat groupChat);
	
	public List<MessageChat> findAllByGroupChatPaging(GroupChat groupChat);
}
