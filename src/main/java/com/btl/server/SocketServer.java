package com.btl.server;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.codehaus.jackson.map.ObjectMapper;

import com.btl.code.MessageChatDecode;
import com.btl.code.MessageChatEncode;
import com.btl.dao.FileUploadDao;
import com.btl.dao.GroupChatDao;
import com.btl.dao.MessageChatDao;
import com.btl.dao.impl.FileUploadDaoImpl;
import com.btl.dao.impl.GroupChatDaoImpl;
import com.btl.dao.impl.MessageChatDaoImpl;
import com.btl.model.FileUpload;
import com.btl.model.GroupChat;
import com.btl.model.MessageChat;

@ServerEndpoint(value = "/server-endpoint", encoders = { MessageChatEncode.class }, decoders = {
		MessageChatDecode.class })
public class SocketServer {

	static File uploadedFile = null;
	static String fileName = null;
	static FileOutputStream fos = null;
	final static String filePath = "E:/New folder/";

	private ObjectMapper mapper = new ObjectMapper();

	private MessageChatDao mesDao = new MessageChatDaoImpl();

	private GroupChatDao grdao = new GroupChatDaoImpl();

	private FileUploadDao fileDao = new FileUploadDaoImpl();

	private static Set<Session> sessions = Collections.synchronizedSet(new HashSet<Session>());

	@OnOpen
	public void onOpen(Session session) throws IOException {
		System.out.println("A Client connect ...");
		sessions.add(session);
		Iterator<Session> iterator = sessions.iterator();
		while (iterator.hasNext()) {
			iterator.next().getBasicRemote().sendText(getJsonUserOnline());
		}
	}

	@OnMessage
	public void onMessage(MessageChat message, Session session) throws IOException, EncodeException {
		Object object = session.getUserProperties().get("userSession");
		if (object == null) {
			session.getUserProperties().put("userSession", message);
			Iterator<Session> iterator = sessions.iterator();
			while (iterator.hasNext()) {
				iterator.next().getBasicRemote().sendText(getJsonUserOnline());
			}
		} else {
			message.setCreateTime(new Timestamp(System.currentTimeMillis()));
			MessageChat out = new MessageChat();
			out.setContent(message.getContent());
			out.setIdUserSend(message.getIdUserSend());
			out.setIdUserReceive(message.getIdUserReceive());
			out.setCreateTime(message.getCreateTime());
			out.setType(message.getType());
			// System.out.println(message.getType());
			Long idMes = null;
			if (message.getRoomChat() != null) {
				idMes = mesDao.insert(message);
				out.setId(idMes);
				sendOneToOne(out);
			} else {
				try {
					GroupChat gr = grdao.findById(message.getIdUserReceive());
					message.setIdGroupChat(gr.getId());
					idMes = mesDao.insert(message);
					out.setId(idMes);
					out.setFullName(message.getFullName());
					sendOneToMany(out, gr.getMember());
				} catch (NullPointerException e) {
					// System.out.println("Vo day");
				}
			}

			if (message.getType() == 2) {
				createDirFile(message, idMes);
			}
		}
	}

	@OnMessage
	public void processUpload(ByteBuffer msg, boolean last, Session session) {
		// System.out.println("Downloading ...");
		while (msg.hasRemaining()) {
			// System.out.println("............");
			try {
				fos.write(msg.get());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@OnClose
	public void onClose(Session session) throws IOException {
		System.out.println("Close ...");
		session.close();
		sessions.remove(session);
		Iterator<Session> iterator = sessions.iterator();
		while (iterator.hasNext()) {
			iterator.next().getBasicRemote().sendText(getJsonUserOnline());
		}
	}

	@OnError
	public void onError(Throwable t) {
		// t.getStackTrace();
		System.err.println(t.getMessage());
	}

	private String getJsonUserOnline() {
		Iterator<MessageChat> iterator = getUsersOnline().iterator();
		JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
		try {
			while (iterator.hasNext()) {
				jsonArrayBuilder.add(mapper.writeValueAsString(iterator.next()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Json.createObjectBuilder().add("users", jsonArrayBuilder).build().toString();
	}

	private Set<MessageChat> getUsersOnline() {
		HashSet<MessageChat> hashSet = new HashSet<MessageChat>();
		Iterator<Session> iterator = sessions.iterator();
		while (iterator.hasNext()) {
			MessageChat temp = null;
			temp = (MessageChat) iterator.next().getUserProperties().get("userSession");
			if (temp != null) {
				hashSet.add(temp);
			}
		}
		return hashSet;
	}

	private void sendOneToOne(MessageChat out) throws IOException, EncodeException {
		Iterator<Session> iterator = sessions.iterator();
		while (iterator.hasNext()) {
			Session ss = iterator.next();
			MessageChat ms = (MessageChat) ss.getUserProperties().get("userSession");
			if (ms.getIdUserSend() == out.getIdUserSend() || ms.getIdUserSend() == out.getIdUserReceive()) {
				ss.getBasicRemote().sendObject(out);
			}
		}
	}

	private void sendOneToMany(MessageChat out, String member) throws IOException, EncodeException {
		Iterator<Session> iterator = sessions.iterator();
		while (iterator.hasNext()) {
			Session ss = iterator.next();
			MessageChat ms = (MessageChat) ss.getUserProperties().get("userSession");
			if (member.contains(ms.getIdUserSend() + "")) {
				ss.getBasicRemote().sendObject(out);
			}
		}
	}

	private void createDirFile(MessageChat message, Long idMes) {
		System.out.println("MSG : " + message.getContent());
		if (!message.getContent().equals(fileName + "_")) {
			fileName = message.getContent();
			uploadedFile = new File(filePath + fileName);
			FileUpload fileUpload = new FileUpload();
			fileUpload.setCreateTime(new Timestamp(System.currentTimeMillis()));
			fileUpload.setCreateBy(message.getIdUserSend() + "");
			fileUpload.setName(fileName);
			fileUpload.setLocation(filePath + fileName);
			fileUpload.setIdMes(idMes);
			Long idFileLong = fileDao.insert(fileUpload);
			String root = filePath + idFileLong + "/";
			fileUpload.setCreateTime(new Timestamp(System.currentTimeMillis()));
			fileUpload.setLocation(root + fileName);
			fileUpload.setId(idFileLong);
			System.out.println(fileUpload.getLocation());
			fileDao.update(fileUpload);

			Path path = Paths.get(root);
			if (!Files.exists(path)) {
				try {
					Files.createDirectories(path);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				fos = new FileOutputStream(root + fileName);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		} else {
			try {
				fos.flush();
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
