package com.btl.mapper.impl;

import java.sql.ResultSet;

import com.btl.mapper.IMapper;
import com.btl.model.GroupChat;

public class GroupChatMapper implements IMapper<GroupChat> {

	@Override
	public GroupChat mapRow(ResultSet rs) {
		GroupChat groupChat = new GroupChat();
		try {
			groupChat.setId(rs.getLong("id"));
			groupChat.setName(rs.getString("name"));
			groupChat.setMember(rs.getString("member"));
			groupChat.setType(rs.getInt("type"));
		} catch (Exception e) {
			System.err.println("Lỗi convert Mapper");
		}
		return groupChat;
	}

}
