package com.btl.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.btl.mapper.IMapper;
import com.btl.model.MessageChat;
import com.btl.model.RoomChat;

public class MessageChatMapper implements IMapper<MessageChat> {

	@Override
	public MessageChat mapRow(ResultSet rs) {
		try {
			MessageChat messageChat = new MessageChat();
			messageChat.setIdUserSend(rs.getLong("idUserSend"));
			messageChat.setContent(rs.getString("content"));
			messageChat.setCreateTime(rs.getTimestamp("createTime"));
			messageChat.setType(rs.getInt("type"));
			try {
				messageChat.setFullName(rs.getString("fullName"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				messageChat.setId(rs.getLong("id"));
				messageChat.setIdGroupChat(rs.getLong("idGroupChat"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				RoomChat roomChat = new RoomChat();
				roomChat.setIdUserOne(rs.getLong("idUserOne"));
				roomChat.setIdUserTwo(rs.getLong("idUserTwo"));
				messageChat.setRoomChat(roomChat);
			} catch (SQLException e) {
				// System.out.println("Lỗi mapper");
			}
			return messageChat;
		} catch (SQLException e) {
			// System.out.println("Lỗi mapper");
			return null;
		}
	}

}
