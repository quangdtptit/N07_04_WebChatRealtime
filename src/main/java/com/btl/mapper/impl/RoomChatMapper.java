package com.btl.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.btl.mapper.IMapper;
import com.btl.model.RoomChat;

public class RoomChatMapper implements IMapper<RoomChat> {

	@Override
	public RoomChat mapRow(ResultSet rs) {
		try {
			RoomChat roomChat = new RoomChat();
			roomChat.setId(rs.getLong("id"));
			roomChat.setIdUserOne(rs.getLong("idUserOne"));
			roomChat.setIdUserTwo(rs.getLong("idUserTwo"));
			roomChat.setCreateTime(rs.getTimestamp("createTime"));
			roomChat.setCreateBy(rs.getString("createBy"));
			return roomChat;
		} catch (SQLException e) {
			return null;
		}
	}

}
