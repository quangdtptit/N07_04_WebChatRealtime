package com.btl.mapper.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.btl.mapper.IMapper;
import com.btl.model.User;

public class UserMappper implements IMapper<User> {

	@Override
	public User mapRow(ResultSet rs) {
		try {
			User userModel = new User();
			userModel.setUserName(rs.getString("username"));
			userModel.setId(rs.getLong("id"));
			userModel.setPassword(rs.getString("password"));
			userModel.setFullName(rs.getString("fullname"));
			userModel.setStatus(rs.getInt("status"));
			return userModel;
		} catch (SQLException e) {
			return null;
		}
	}

}
