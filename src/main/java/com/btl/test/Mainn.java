package com.btl.test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.Locale;

public class Mainn {
	public static void main(String[] args) {
		int week = 1;
		int month = 11;
		int year = 2019;
		
		WeekFields weekFields = WeekFields.of(Locale.getDefault());
		
		LocalDate today = LocalDate.now()
				.withYear(2019)
				.with(weekFields.weekOfMonth(),week)
				.withMonth(month);

	    LocalDate monday = today;
	    
	    while (monday.getDayOfWeek() != DayOfWeek.MONDAY)
	    {
	      monday = monday.minusDays(1);
	    }

	    LocalDate sunday = today;
	    while (sunday.getDayOfWeek() != DayOfWeek.SUNDAY)
	    {
	      sunday = sunday.plusDays(1);
	    }

	    System.out.println("Today: " + today);
	    System.out.println("Monday of the Week: " + monday);
	    System.out.println("Sunday of the Week: " + sunday);
	}
}

