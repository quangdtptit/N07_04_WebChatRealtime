package com.btl.test;

import java.net.URI;

import javax.websocket.ClientEndpoint;
import javax.websocket.ContainerProvider;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

import com.btl.code.MessageChatDecode;
import com.btl.code.MessageChatEncode;
import com.btl.model.MessageChat;

@ClientEndpoint(decoders = { MessageChatDecode.class }, encoders = { MessageChatEncode.class })
public class ClientData {

	private Session session = null;

	public ClientData() {
		try {
			URI uri = new URI("ws://localhost:8080/server-endpoint");
			ContainerProvider.getWebSocketContainer().connectToServer(this, uri);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@OnOpen
	public void onOpen(Session session) {
		this.session = session;
		System.out.println("Client connect by session: "+session.getId());
	}

	@OnMessage
	public void onMessage(MessageChat messageChat, Session session) {

	}

	@OnClose
	public void onClose(Session session) {

	}

	@OnError
	public void onError(Throwable t) {
		System.out.println(t.getMessage());
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

}
