/*
Navicat MySQL Data Transfer

Source Server         : localhost_mysql
Source Server Version : 80012
Source Host           : localhost:3306
Source Database       : chatchit

Target Server Type    : MYSQL
Target Server Version : 80012
File Encoding         : 65001

Date: 2019-10-17 23:05:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `fileupload`
-- ----------------------------
DROP TABLE IF EXISTS `fileupload`;
CREATE TABLE `fileupload` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `createtime` timestamp NULL DEFAULT NULL,
  `idMes` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idMes` (`idMes`),
  CONSTRAINT `fileupload_ibfk_1` FOREIGN KEY (`idMes`) REFERENCES `messagechat` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of fileupload
-- ----------------------------

-- ----------------------------
-- Table structure for `groupchat`
-- ----------------------------
DROP TABLE IF EXISTS `groupchat`;
CREATE TABLE `groupchat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `member` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) DEFAULT NULL,
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createBy` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of groupchat
-- ----------------------------
INSERT INTO `groupchat` VALUES ('13', 'Lập trình mạng', '1-3-5', '2', '2019-09-29 23:36:17', 'admin');
INSERT INTO `groupchat` VALUES ('14', 'Lập trình javaWeb', '1-2', '2', '2019-09-30 13:51:44', null);
INSERT INTO `groupchat` VALUES ('15', 'Cơ sở dữ liệu phân tán', '1-5', '2', '2019-09-30 22:04:20', 'admin');
INSERT INTO `groupchat` VALUES ('16', 'Công nghệ phần mềm', '1-5', '2', '2019-10-03 20:22:27', 'admin');
INSERT INTO `groupchat` VALUES ('17', 'Nhóm tầng 5', '1-4', '2', '2019-10-06 21:10:15', 'thanhlong');
INSERT INTO `groupchat` VALUES ('18', 'New Nhóm', '1-3', '2', '2019-10-13 08:25:52', 'admin');

-- ----------------------------
-- Table structure for `messagechat`
-- ----------------------------
DROP TABLE IF EXISTS `messagechat`;
CREATE TABLE `messagechat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idUserSend` bigint(20) NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) DEFAULT NULL,
  `idRoomChat` bigint(20) DEFAULT NULL,
  `createTime` timestamp NULL DEFAULT NULL,
  `idGroupChat` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idRoomChat` (`idRoomChat`),
  KEY `idGroupChat` (`idGroupChat`),
  CONSTRAINT `messagechat_ibfk_1` FOREIGN KEY (`idRoomChat`) REFERENCES `roomchat` (`id`),
  CONSTRAINT `messagechat_ibfk_2` FOREIGN KEY (`idGroupChat`) REFERENCES `groupchat` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of messagechat
-- ----------------------------
INSERT INTO `messagechat` VALUES ('142', '1', 'Hello Do Thanh Quang', '1', '14', '2019-10-11 12:19:50', null);
INSERT INTO `messagechat` VALUES ('143', '1', 'Mình là Quang thành viên nhóm Lập trình mạng', '1', null, '2019-10-12 20:23:33', '13');
INSERT INTO `messagechat` VALUES ('144', '1', 'Mình là Quang thành viên nhóm Lập trình javaWev', '1', null, '2019-10-12 20:23:50', '14');
INSERT INTO `messagechat` VALUES ('145', '1', 'Mình là Quang thành viên nhóm CSDL phân tán', '1', null, '2019-10-12 20:25:02', '15');
INSERT INTO `messagechat` VALUES ('146', '1', 'Mình là Quang thành viên nhóm Công nghệ phần mềm', '1', null, '2019-10-12 20:25:29', '16');
INSERT INTO `messagechat` VALUES ('147', '1', 'Nay có mang chìa khóa đi không', '1', null, '2019-10-12 20:25:42', '17');
INSERT INTO `messagechat` VALUES ('148', '5', 'Alo Quang ơi', '1', '15', '2019-10-12 20:28:47', null);
INSERT INTO `messagechat` VALUES ('149', '1', 'Gì z ông', '1', '15', '2019-10-12 20:28:57', null);
INSERT INTO `messagechat` VALUES ('150', '5', 'Ông làm bt về nhà cưa', '1', '15', '2019-10-12 20:29:42', null);
INSERT INTO `messagechat` VALUES ('151', '5', 'chưa', '1', '15', '2019-10-12 20:29:48', null);
INSERT INTO `messagechat` VALUES ('152', '5', 'môn lập trình web ý', '1', '15', '2019-10-12 20:29:56', null);
INSERT INTO `messagechat` VALUES ('153', '1', 'bài tập gì z hông biết lun', '1', '15', '2019-10-12 20:30:15', null);
INSERT INTO `messagechat` VALUES ('154', '1', 'Alo m.n', '1', null, '2019-10-12 20:30:48', '13');
INSERT INTO `messagechat` VALUES ('155', '1', 'Alo', '1', null, '2019-10-12 20:31:00', '13');
INSERT INTO `messagechat` VALUES ('156', '1', 'Alo', '1', null, '2019-10-12 20:31:55', '13');
INSERT INTO `messagechat` VALUES ('157', '5', 'Hả', '1', null, '2019-10-12 20:35:58', '13');
INSERT INTO `messagechat` VALUES ('158', '5', 'Chan', '1', '15', '2019-10-12 20:36:12', null);
INSERT INTO `messagechat` VALUES ('159', '5', 'Alo', '1', null, '2019-10-12 20:41:15', '13');
INSERT INTO `messagechat` VALUES ('160', '5', 'Alo tôi là Quý', '1', null, '2019-10-12 20:47:40', '13');
INSERT INTO `messagechat` VALUES ('161', '5', 'Ông phân công đi', '1', null, '2019-10-12 20:47:52', '13');
INSERT INTO `messagechat` VALUES ('162', '1', 'chịu thôi', '1', '15', '2019-10-12 20:48:19', null);
INSERT INTO `messagechat` VALUES ('163', '1', 'Biết làm sao h -_-', '1', '15', '2019-10-12 20:48:27', null);
INSERT INTO `messagechat` VALUES ('164', '5', 'ok ô', '1', '15', '2019-10-12 20:48:33', null);
INSERT INTO `messagechat` VALUES ('165', '1', 'cc', '1', '15', '2019-10-12 20:48:38', null);
INSERT INTO `messagechat` VALUES ('166', '3', 'Hello cậu', '1', '2', '2019-10-13 08:24:32', null);
INSERT INTO `messagechat` VALUES ('167', '1', 'Yep', '1', '2', '2019-10-13 08:24:35', null);
INSERT INTO `messagechat` VALUES ('168', '3', 'Hello mọi mình là ĐQA', '1', null, '2019-10-13 08:24:52', '13');
INSERT INTO `messagechat` VALUES ('169', '1', 'Chào cậu', '1', null, '2019-10-13 08:24:58', '13');
INSERT INTO `messagechat` VALUES ('170', '1', 'Nhóm mới nè', '1', null, '2019-10-13 08:26:05', '18');
INSERT INTO `messagechat` VALUES ('171', '1', 'Alo', '1', null, '2019-10-13 08:26:12', '18');
INSERT INTO `messagechat` VALUES ('172', '1', 'Ok cậu', '1', null, '2019-10-13 08:26:36', '18');
INSERT INTO `messagechat` VALUES ('173', '3', 'Ok cậu', '1', null, '2019-10-13 08:26:46', '18');
INSERT INTO `messagechat` VALUES ('174', '1', 'Hello c', '1', '2', '2019-10-13 10:01:00', null);
INSERT INTO `messagechat` VALUES ('175', '3', 'jjjj', '1', '2', '2019-10-13 10:02:00', null);
INSERT INTO `messagechat` VALUES ('176', '1', 'Tesrt', '1', null, '2019-10-13 10:02:29', '13');
INSERT INTO `messagechat` VALUES ('177', '1', 'client.txt', '2', null, '2019-10-13 10:08:51', '13');

-- ----------------------------
-- Table structure for `roomchat`
-- ----------------------------
DROP TABLE IF EXISTS `roomchat`;
CREATE TABLE `roomchat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idUserOne` bigint(20) NOT NULL,
  `idUserTwo` bigint(20) NOT NULL,
  `createTime` timestamp NULL DEFAULT NULL,
  `createBy` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of roomchat
-- ----------------------------
INSERT INTO `roomchat` VALUES ('1', '2', '1', null, null);
INSERT INTO `roomchat` VALUES ('2', '1', '3', null, null);
INSERT INTO `roomchat` VALUES ('3', '2', '3', null, null);
INSERT INTO `roomchat` VALUES ('9', '3', '3', '2019-09-15 00:08:04', '');
INSERT INTO `roomchat` VALUES ('10', '2', '2', '2019-09-15 00:13:01', '');
INSERT INTO `roomchat` VALUES ('14', '1', '1', '2019-09-15 21:17:25', '');
INSERT INTO `roomchat` VALUES ('15', '1', '5', '2019-09-22 20:01:56', '');
INSERT INTO `roomchat` VALUES ('16', '5', '5', '2019-09-22 20:17:23', '');
INSERT INTO `roomchat` VALUES ('17', '3', '5', '2019-10-01 14:56:20', '');
INSERT INTO `roomchat` VALUES ('18', '1', '4', '2019-10-06 21:09:36', '');
INSERT INTO `roomchat` VALUES ('19', '6', '1', '2019-10-11 12:13:42', '');
INSERT INTO `roomchat` VALUES ('20', '6', '6', '2019-10-11 12:13:58', '');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `createtime` timestamp NULL DEFAULT NULL,
  `createby` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '$2a$10$6xc3Qv/Bp4pbRDoFQJNA3evafjuKBOLrmPE6C.yYEvbD5pcKNnv02', 'Đỗ Thanh Quang', '1', null, null);
INSERT INTO `user` VALUES ('2', 'quangdt', '$2a$10$XKW5tlonJPlk9nFSfMFqB.KmCa1SY.SfDVI24/oKNzYY3BDRUYldu', 'Đỗ Thanh Tiến', '1', null, null);
INSERT INTO `user` VALUES ('3', 'queanh', '$2a$10$/RHOGf.Q7bYgwrVZIu7.4O/5sp/0pIOMoLoxOLJKooyLL3.6.C1XG', 'Đặng Quế Anh', '1', null, null);
INSERT INTO `user` VALUES ('4', 'thanhlong', '$2a$10$9PVAUvdyjxiGnhbz/u1EUudi2BlBiAW5RooYaDiio3Sxt3Q/SwdJ2', 'Nguyễn Thành Long', '1', null, null);
INSERT INTO `user` VALUES ('5', 'quyvn', '$2a$10$jhrdvd1rp.mvX282hmg/aeLa.ac2IVG79GtVuyLWnhJCUecKFVhmu', 'Vũ Ngọc Quý', '1', null, null);
