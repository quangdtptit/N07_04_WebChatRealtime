$(document).ready(function() {
	
	const port = "192.168.1.104";

    const _idUserSend = $('#idUserSend').val();

    const socket = new WebSocket("ws://"+port+"/server-endpoint");

    socket.binaryType = "arraybuffer";

    var _typeSend = 0; //0 : sendString | 1 : sendFile

    var roomId = _idUserSend;

    var gr = temp();

    var fileName;
    
    var page = 1;

    function temp() {
        var strResult = '';
        $.each($("#listGr"), function(index, value) {
            strResult += value.innerHTML;
        });
        return strResult;
    }

    document.getElementById('inputFile').onchange = function() {
        var arrs = this.value.split("\\");
        fileName = arrs[arrs.length - 1];
        $('#txtMessage').val(fileName);
        _typeSend = 1;
    };
    
    function checkNotify(jsonData) {
    	var idUserSend = jsonData.idUserSend;
    	var idUserReceive = jsonData.idUserReceive;
    	var fullName = jsonData.fullName;
    	if(fullName == null || fullName == 'undefined'){
	    	var listUserOnline = $("#listOnline li");
	        listUserOnline.each(function(index, li) {
	            var item = $(li);
	            var idUserOnline = $(item).data('id').split("-");
	            if (!item.is(".active") && idUserReceive == _idUserSend && idUserSend == idUserOnline[1]) {
	                item.children().find("p").text('Có tin nhắn mới ...');
	                //console.log(item);
	                return;
	            }     
	        })
    	}else{
    		var listUserOnline = $("#listOnline li");
	        listUserOnline.each(function(index, li) {
	            var item = $(li);
	            var idUserOnline = $(item).data('id').split("-");
	            if (!item.is(".active") && idUserReceive == idUserOnline[1]) {
	                item.children().find("p").text('Có tin nhắn mới ...');
	                //console.log(item);
	                return;
	            }     
	        })
    	}
	}

    //OnOpen
    socket.onopen = function(event) {
        online();
    };

    function online() {
        var roomChat = new RoomChat();
        roomChat.id = roomId;
        var messageChat = new MessageChat();
        messageChat.fullName = $('#idSession').val();
        messageChat.idUserSend = _idUserSend;
        socket.send(JSON.stringify(messageChat));
    };

    //sendObject()
    socket.onmessage = function(message) {
        var jsonData = JSON.parse(message.data);
        if (jsonData.users != null) {
            $('#listOnline').empty();
            var i = 0;
            var elements = '';
            while (i < jsonData.users.length) {
                var object = JSON.parse(jsonData.users[i]);
                elements += '<li data-id="a-' + object.idUserSend + '" style="cursor: pointer;" class="">' +
                    '<div class="d-flex bd-highlight">' +
                    '<div class="img_cont">' +
                    '<img src="/template/images/avt.jpg" class="rounded-circle user_img">' +
                    '<span class="online_icon"></span>' +
                    '</div>' +
                    '<div class="user_info">' +
                    '<span id="name-id-a-' + object.idUserSend + '">' + object.fullName + '</span>' +
                    '<p></p>' +
                    '</div>' +
                    '</div>' +
                    '</li>';
                i++;
            }
            elements += gr;
            $('#listOnline').append(elements);
        }
        
        if (jsonData.idUserSend != null && jsonData.idUserReceive != null) {
            var idUserSend = jsonData.idUserSend;
            var text;
            var url = window.location.href.split("/");
            checkNotify(jsonData);
            if (jsonData.fullName == null || jsonData.fullName == 'undefined') {
            	//1-1
            	if(_idUserReceive = url[url.length - 1].split("-")[0] == 'a'){
	                var idUserReceive = jsonData.idUserReceive;
	                var _idUserReceive = url[url.length - 1].split("-")[1];
	                if (idUserSend == _idUserSend && idUserReceive == _idUserReceive) {
	                    text = '<div class="d-flex justify-content-end mb-4">' +
	                        '<div class="msg_cotainer_send">' + jsonData.content +
	                        '<span class="msg_time_send" style="width:110px;height:15px;">' + moment(jsonData.createTime).format('h:mm A, D/MM/YYYY ') + '</span>' +
	                        '</div>' +
	                        '<div class="img_cont_msg">' +
	                        '<img src="/template/images/avt.jpg" class="rounded-circle user_img_msg">' +
	                        '</div>' +
	                        '</div>';
	                } else {
	                    if (idUserSend == _idUserReceive && idUserReceive == _idUserSend) {
	                        text = '<div class="d-flex justify-content-start mb-4">' +
	                            '<div class="img_cont_msg">' +
	                            '<img src="/template/images/avt.jpg" class="rounded-circle user_img_msg">' +
	                            '</div>' +
	                            '<div class="msg_cotainer">' + jsonData.content +
	                            '<span class="msg_time" style="width:110px;height:15px;">' + moment(jsonData.createTime).format('h:mm A, D/MM/YYYY ') + '</span>' +
	                            '</div>' +
	                            '</div>';
	                    }
	                }
            	}
            } else {
            	//1-n
            	if(_idUserReceive = url[url.length - 1].split("-")[0] == 'gr'){
	                var _arrName = jsonData.fullName.split(" ");
	                var _name = _arrName[_arrName.length - 1];
	                if (idUserSend == _idUserSend) {
	                    text = '<div class="d-flex justify-content-end mb-4">' +
	                        '<div class="msg_cotainer_send">' + jsonData.content +
	                        '<span class="msg_time_send" style="width:110px;height:15px;">' + moment(jsonData.createTime).format('h:mm A, D/MM/YYYY ') + '</span>' +
	                        '</div>' +
	                        '<div class="img_cont_msg">' +
	                        '<img src="/template/images/avt.jpg" class="rounded-circle user_img_msg">' +
	                        '</div>' +
	                        '</div>';
	                } else {
	                    text = '<div class="d-flex justify-content-start mb-4">' +
	                        '<div class="img_cont_msg">' +
	                        '<img src="/template/images/avt.jpg" class="rounded-circle user_img_msg">' +
	                        '</div>' +
	                        '<div class="msg_cotainer">' + jsonData.content +
	                        '<span class="msg_time" style="width:110px;height:15px;">' + _name + ', ' + moment(jsonData.createTime).format('h:mm A, D/MM/YYYY ') + '</span>' +
	                        '</div>' +
	                        '</div>';
	                }
            	}
            }
            $('#displayChat').append(text);
            $("#displayChat").scrollTop($('#displayChat')[0].scrollHeight);
        }
    };



    function sendMes() {
        var url = window.location.href.split("/");
        var idUserReceive = url[url.length - 1];
        var content = $('#txtMessage').val();
        if (content === "undefined" || content.trim() == "" || idUserReceive === "undefined") {
            return;
        } else {
            var strArrs = idUserReceive.split("-");
            var messageChat = new MessageChat();
            messageChat.fullName = $("#fullName").text();
            messageChat.content = content;
            messageChat.idUserSend = _idUserSend;
            messageChat.idUserReceive = strArrs[1];
            messageChat.type = 1;
            if (_typeSend == 1) {
                messageChat.type = 2;
            }
            if (strArrs[0] == 'a') {
                var roomChat = new RoomChat();
                roomChat.id = roomId;
                messageChat.roomChat = roomChat;
            }
            socket.send(JSON.stringify(messageChat));
        }
        $('#txtMessage').val("");
    }

    $("#txtMessage").keypress(function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            e.preventDefault();
            sendMes();
            if (_typeSend == 1) {
                sendFile();
            }
            _typeSend = 0;
        }
    });

    window.onbeforeunload = function() {
        //@OnClose
        socket.onclose = function() {};
        socket.close();
    };

    function loadMessages(result1) {
        var texts = '';
        for (let i = 0, len = result1.length, text = ""; i < len; i++) {
            if (result1[i].idUserSend == _idUserSend) {
                texts += '<div class="d-flex justify-content-end mb-4">' +
                    '<div class="msg_cotainer_send">' + result1[i].content +
                    '<span class="msg_time_send" style="width:110px;height:15px;">' + moment(result1[i].createTime).format('h:mm A, D/MM/YYYY ') + '</span>' +
                    '</div>' +
                    '<div class="img_cont_msg">' +
                    '<img src="/template/images/avt.jpg" class="rounded-circle user_img_msg">' +
                    '</div>' +
                    '</div>';
            } else {
                texts += '<div class="d-flex justify-content-start mb-4">' +
                    '<div class="img_cont_msg">' +
                    '<img src="/template/images/avt.jpg" class="rounded-circle user_img_msg">' +
                    '</div>' +
                    '<div class="msg_cotainer">' + result1[i].content +
                    '<span class="msg_time" style="width:110px;height:15px;">' + moment(result1[i].createTime).format('h:mm A, D/MM/YYYY ') + '</span>' +
                    '</div>' +
                    '</div>';
            }
        }
        $('#displayChat').append(texts);
        $("#displayChat").scrollTop($('#displayChat')[0].scrollHeight);
    }

    function loadMesGroup(result1) {
        var texts = '';
        var _name;
        var _arrName;
        for (let i = 0, len = result1.length, text = ""; i < len; i++) {
            if (result1[i].idUserSend == _idUserSend) {
                texts += '<div class="d-flex justify-content-end mb-4">' +
                    '<div class="msg_cotainer_send">' + result1[i].content +
                    '<span class="msg_time_send" style="width:110px;height:15px;">' + moment(result1[i].createTime).format('h:mm A, D/MM/YYYY ') + '</span>' +
                    '</div>' +
                    '<div class="img_cont_msg">' +
                    '<img src="/template/images/avt.jpg" class="rounded-circle user_img_msg">' +
                    '</div>' +
                    '</div>';
            } else {
                _arrName = result1[i].fullName.split(" ");
                _name = _arrName[_arrName.length - 1];
                texts += '<div class="d-flex justify-content-start mb-4">' +
                    '<div class="img_cont_msg">' +
                    '<img src="/template/images/avt.jpg" class="rounded-circle user_img_msg">' +
                    '</div>' +
                    '<div class="msg_cotainer">' + result1[i].content +
                    '<span class="msg_time" style="width:140px;height:15px;">' + _name + ', ' + moment(result1[i].createTime).format('h:mm A, D/MM/YYYY ') + '</span>' +
                    '</div>' +
                    '</div>';
            }
        }
        $('#displayChat').append(texts);
        $("#displayChat").scrollTop($('#displayChat')[0].scrollHeight);
    }

    $(document).on('click', '#isGr', function(e) {
        $.ajax({
            dataType: 'json',
            url: '/api/user',
            type: 'GET',
            contentType: 'application/json',
            success: function(result) {
                $("#listFriend > tbody").empty();
                var rows = '';
                $.each(result, function(index, value) {
                    rows += '<tr>' +
                        '<th scope="row">' +
                        '<div class="custom-control custom-checkbox">' +
                        '<input type="checkbox" name="checkMember" class="custom-control-input" id="' + value.id + '">' +
                        '<label class="custom-control-label" for="' + value.id + '"></label>' +
                        '</div>' +
                        '</th>' +
                        '<td>' + value.fullName + '</td>' +
                        '</tr>';
                });
                $('#listFriend tbody').append(rows);
            },
            error: function(error) {
                console.log(error);
                alert(error);
            }
        });
    });

    $('#btnAddGr').click(function(e) {
        e.preventDefault();
        var members = [];
        $.each($("input[name='checkMember']:checked"), function() {
            members.push($(this).attr("id"));
        });
        var data = {};
        data["member"] = members.join("-");
        data["name"] = $("#nameGroup").val();
        data["type"] = 2;

        $.ajax({
            dataType: 'json',
            url: '/api/group-chat',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function(result) {
                alert("Thêm thành công nhóm " + result.name);
                window.location.href = "http://"+port+"/messages/t/" + _idUserSend;
            },
            error: function(error) {
                console.log(error);
                alert(error);
            }
        });
    });

    function sendFile() {
        var data = {
            content: fileName + '_',
            type: 2
        };
        var file = document.getElementById('inputFile').files[0];
        var reader = new FileReader();
        var rawData = new ArrayBuffer();
        reader.loadend = function() {}
        reader.onload = function(e) {
            rawData = e.target.result;
            socket.send(rawData);
            socket.send(JSON.stringify(data))
        }
        reader.readAsArrayBuffer(file);
    }

    //scroll event
    $("#displayChat").on('scroll', function() {
        var scroll = $(this).scrollTop();
        var firstMsg = $('.mb-4:first');
        var curOffset = firstMsg.offset().top - $(this).scrollTop();
        if (scroll < 1) {
        	var url = window.location.href.split("/");
        	var ids = url[url.length - 1].split("-");
        	if(ids[0] == 'a'){
        		var data = {};
            	data["page"] = page;
            	data["idUserTwo"] = _idUserSend;
        		$.ajax({
                    dataType: 'json',
                    url: '/api/messages/' + ids[1],
                    type: 'PUT',
                    contentType: 'application/json',
                    data: JSON.stringify(data),
                    success: function(result1) {
                        if (result1 != null && result1 !== "undefined") {
                        	var texts = '';
                            for (let i = 0, len = result1.length, text = ""; i < len; i++) {
                                if (result1[i].idUserSend == _idUserSend) {
                                    texts += '<div class="d-flex justify-content-end mb-4">' +
                                        '<div class="msg_cotainer_send">' + result1[i].content +
                                        '<span class="msg_time_send" style="width:110px;height:15px;">' + moment(result1[i].createTime).format('h:mm A, D/MM/YYYY ') + '</span>' +
                                        '</div>' +
                                        '<div class="img_cont_msg">' +
                                        '<img src="/template/images/avt.jpg" class="rounded-circle user_img_msg">' +
                                        '</div>' +
                                        '</div>';
                                } else {
                                    texts += '<div class="d-flex justify-content-start mb-4">' +
                                        '<div class="img_cont_msg">' +
                                        '<img src="/template/images/avt.jpg" class="rounded-circle user_img_msg">' +
                                        '</div>' +
                                        '<div class="msg_cotainer">' + result1[i].content +
                                        '<span class="msg_time" style="width:110px;height:15px;">' + moment(result1[i].createTime).format('h:mm A, D/MM/YYYY ') + '</span>' +
                                        '</div>' +
                                        '</div>';
                                }
                            }
                        	$("#displayChat").prepend(texts);
                            $("#displayChat").scrollTop(firstMsg.offset().top - curOffset);
                            page += 10;
                        }
                    },
                    error: function(error) {
                        console.log(error);
                        alert(error);
                    }
                });  
        	}else{
        		var data = {};
            	data["page"] = page;
        		$.ajax({
                    dataType: 'json',
                    url: '/api/messages/' + ids[1],
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(data),
                    success: function(result1) {
                        if (result1 != null && result1 !== "undefined") {
                        	var texts = '';
                            var _name;
                            var _arrName;
                            for (let i = 0, len = result1.length, text = ""; i < len; i++) {
                                if (result1[i].idUserSend == _idUserSend) {
                                    texts += '<div class="d-flex justify-content-end mb-4">' +
                                        '<div class="msg_cotainer_send">' + result1[i].content +
                                        '<span class="msg_time_send" style="width:110px;height:15px;">' + moment(result1[i].createTime).format('h:mm A, D/MM/YYYY ') + '</span>' +
                                        '</div>' +
                                        '<div class="img_cont_msg">' +
                                        '<img src="/template/images/avt.jpg" class="rounded-circle user_img_msg">' +
                                        '</div>' +
                                        '</div>';
                                } else {
                                    _arrName = result1[i].fullName.split(" ");
                                    _name = _arrName[_arrName.length - 1];
                                    texts += '<div class="d-flex justify-content-start mb-4">' +
                                        '<div class="img_cont_msg">' +
                                        '<img src="/template/images/avt.jpg" class="rounded-circle user_img_msg">' +
                                        '</div>' +
                                        '<div class="msg_cotainer">' + result1[i].content +
                                        '<span class="msg_time" style="width:140px;height:15px;">' + _name + ', ' + moment(result1[i].createTime).format('h:mm A, D/MM/YYYY ') + '</span>' +
                                        '</div>' +
                                        '</div>';
                                }
                            }
                        	$("#displayChat").prepend(texts);
                            $("#displayChat").scrollTop(firstMsg.offset().top - curOffset);
                            page += 10;
                        }
                    },
                    error: function(error) {
                        console.log(error);
                        alert(error);
                    }
                });
        	}         
        }
    });

    //sự kiện click friend
    $(document).on('click', '#listOnline li', function(e) {
        if ($(this).hasClass("active") == false) { // nếu như click "not active" thì ms send Ajax
        	page = 1;
            $("#listOnline li").parent().find('li').removeClass("active");
            $(this).addClass("active");
            var link = "/messages/t/" + $(this).data('id');
            window.history.pushState("http://"+port+"/home", "", link);
            $(this).children().find("p").text('');
            var roomChat = new RoomChat();
            roomChat.idUserTwo = _idUserSend;
            var _id = $(this).data('id');
            var name = $("#name-id-" + _id).text();
            $('#chatWith').text(name);
            var ids = _id.split("-");
            if (ids[0] == 'gr') {
                $("#imageChat").attr("src", "/template/images/group.jpg");
                $('#displayChat').empty();
                $.ajax({
                    dataType: 'json',
                    url: '/api/messages/' + ids[1],
                    type: 'POST',
                    contentType: 'application/json',
                    success: function(result1) {
                        if (result1 != null && result1 !== "undefined") {
                        	page += 10;
                            loadMesGroup(result1);
                        }
                    },
                    error: function(error) {
                        console.log(error);
                        alert(error);
                    }
                });
            } else {
                $("#imageChat").attr("src", "/template/images/avt.jpg");
                $.ajax({
                    dataType: 'json',
                    url: '/api/roomchat/' + ids[1],
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(roomChat),
                    success: function(result) {
                        $('#displayChat').empty();
                        if (typeof result === 'number') {
                            roomId = result;                     
                            $.ajax({
                                dataType: 'json',
                                url: '/api/messages/' + ids[1],
                                type: 'PUT',
                                contentType: 'application/json',
                                data: JSON.stringify(roomChat),
                                success: function(result1) {
                                    if (result1 != null && result1 !== "undefined") {
                                    	page += 10;
                                        loadMessages(result1);
                                    }
                                },
                                error: function(error) {
                                    console.log(error);
                                    alert(error);
                                }
                            });
                        }
                        if (typeof result === 'object') {
                            roomId = result.id;
                        }
                    },
                    error: function(error) {
                        console.log(error);
                        alert(error);
                    }
                });
            }
        }
    });

});